@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @include('flash::message')
            <div class="card">
                <div class="card-header">Create Transaction</div>

                <div class="card-body">
                    {!! Form::open(['route' => 'transaction.store']) !!}
                        
                        <input type="hidden" name="branch_id" value="1">

                        <div class="form-group">
                            <label>Direct/Indirect/Capex/Transfer</label>
                            {!! Form::select('dic',['direct' => 'Direct', 'indirect' => 'Indirect', 'capex' => 'Capex', 'transfer' => 'Transfer'], null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            <label>Date</label>
                            {!! Form::date('date', \Carbon\Carbon::now(),['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            <label>Effective Date</label>
                            {!! Form::date('effective_date', \Carbon\Carbon::now(),['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            <label>Description</label>
                            {!! Form::text('desc', null,['class' => 'form-control','required'=>true]) !!}
                        </div>

                        <div class="form-group">
                            <label>From</label>
                            {!! Form::text('from', null,['class' => 'form-control','required'=>true]) !!}
                        </div>

                        <div class="form-group">
                            <label>To</label>
                            {!! Form::text('to_company', null,['class' => 'form-control','required'=>true]) !!}
                        </div>


                        <div class="form-group">
                            <label>Quantity</label>
                            {!! Form::number('qty', null,['class' => 'form-control', 'id' => 'qty', 'onchange' => 'calculateTotalAmount()']) !!}
                        </div>

                        <div class="form-group">
                            <label>Unit</label>
                            {!! Form::text('unit', null,['class' => 'form-control', 'step' => '0.01']) !!}
                        </div>

                        <div class="form-group">
                            <label>Unit Price</label>
                            {!! Form::number('unit_price', null,['class' => 'form-control', 'step' => '0.01', 'id' => 'unit_price', 'onchange' => 'calculateTotalAmount()']) !!}
                        </div>

                        <div class="form-group">
                            <label>Amount</label>
                            {!! Form::number('amount', null,['class' => 'form-control', 'step' => '0.01', 'id' => 'amount','required'=>true]) !!}
                        </div>
                        
                        <br>

                        <button class="btn btn-block btn-primary" type="submit">Submit</button>


                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function calculateTotalAmount()
    {
        let qty = document.getElementById('qty').value;
        let unit_price = document.getElementById('unit_price').value;

        let total = qty * unit_price;

        document.getElementById('amount').value = total;
    }
</script>
@endsection
