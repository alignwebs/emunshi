@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
            

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Calculate</div>
                <div class="card-body">
                    {!! Form::open(['route'=>'pl.calculate','class'=>'form-material','id'=>'calucalteProfitLoss']) !!}
                        
                        <div class="row">
                            <div class="col-md-4">
                                <input type="date" name="from" class="form-control" placeholder="From">
                            </div>
                             <div class="col-md-4">
                                <input type="date" name="to" class="form-control" placeholder="To">
                            </div>
                            <div class="col-md-4">
                                <input type="submit" name="calculate" value="Go" class="btn btn-primary">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <br>
            <div id="result">
            </div>

        </div>
    </div>
</div>

<script type="text/javascript"></script>

<script type="text/javascript">

$(document).ready(function(){
    $('#calucalteProfitLoss').submit(function () {

       $.post('{{ route("pl.calculate") }}',$(this).serialize(), function (data){
            $("#result").html(data);
       });
        return false;
    });
});
</script>
@endsection
