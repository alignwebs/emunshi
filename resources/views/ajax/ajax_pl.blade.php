            <div class="card">
                <div class="card-header">Sale</div>
                <div class="card-body">
                    <table class="table table-bordered">
                        @php
                            $sum = 0;
                        @endphp
                        @foreach($data['sales'] as $sale)
                            <tr>
                                <th>{{ $sale['sub_type'] }}</th>
                                <td>{{ $sale['balance'] }}</td>
                            </tr>
                            @php
                                $sum+= $sale['balance'];
                            @endphp
                        @endforeach
                        <tfoot>
                            <th>Total</th>
                            <td>{{ $sum }}</td>
                        </tfoot>
                    </table>
                </div>
            </div>
            <br>
                    <div class="card">
                        <div class="card-header">Direct Expense</div>

                        <div class="card-body">
                            <table class="table table-bordered">
                                
                                    <tr>
                                        <th>Starting Balance</th>
                                        <td>{{ $data['startingBalance'] }}</td>
                                    </tr>
                                     <tr>
                                        <th>Ending Balance</th>
                                        <td>{{ $data['endingingBalance'] }}</td>
                                    </tr>
                                     <tr>
                                        <th>Purchase</th>
                                        <td>{{ $data['purchase']->balance }}</td>
                                    </tr>
                                     <tr>
                                        <th>Total</th>
                                        <td>{{ $data['totalDirectExpense'] }}</td>
                                    </tr>
                                
                            </table>
                        </div>
                    </div>
    <br>
            <div class="card">
                <div class="card-header">Profit</div>

                <div class="card-body">
                    <table class="table table-bordered">
                            @php
                                $profit = $sum - $data['totalDirectExpense'];
                            @endphp
                            <tr>
                                <th>Profit</th>
                                <td>{{ $profit }}</td>
                            </tr>
                       
                    </table>
                </div>
            </div>
    <br>
            <div class="card">
                <div class="card-header">Indirect Expense</div>

                <div class="card-body">
                    <table class="table table-bordered">
                        @php
                            $sumIndirectExpense = 0;
                        @endphp
                        @foreach($data['indirectExpenses'] as $ie)
                            <tr>
                                <th>{{ $ie->from }}</th>
                                <td>{{ $ie->balance }}</td>
                            </tr>
                            @php
                                $sumIndirectExpense+= $ie->balance;
                            @endphp
                        @endforeach
                            @php
                                $sumIndirectExpense+= $data['salary']->balance;
                            @endphp
                            <tr>
                                <th>Salary</th>
                                <td>{{ $data['salary']->balance }}</td>
                            </tr>
                            <tfoot>
                                <th>Total</th>
                                <td>{{ $sumIndirectExpense }}</td>
                            </tfoot>
                    </table>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-header">Net Profit</div>

                <div class="card-body">
                    <table class="table table-bordered">
                       
                            <tr>
                                <th>Gross Profit</th>
                                <td>{{ $profit - $sumIndirectExpense }}</td>
                            </tr>
                       
                    </table>
                </div>
            </div>