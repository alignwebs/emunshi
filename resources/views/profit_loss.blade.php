@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Cash in hand</div>
                <div class="card-body">
                    <table class="table table-bordered">
                        @foreach($data['cashinhand'] as $cashinhand)
                            <tr>
                                <th>{{ $cashinhand['party'] }}</th>
                                <td>{{ $cashinhand['amount'] }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
    <br>
            <div class="card">
                <div class="card-header">Vendor Balance</div>

                <div class="card-body">
                    <table class="table table-bordered">
                        @foreach($data['cashflow'] as $cashflow)
                            <tr>
                                <th>{{ $cashflow->company }}</th>
                                <td>{{ $cashflow->balance }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
    <br>
            <div class="card">
                <div class="card-header">Payroll</div>

                <div class="card-body">
                    <table class="table table-bordered">
                        @foreach($data['payroll'] as $cashflow)
                            <tr>
                                <th>{{ $cashflow->employee }}</th>
                                <td>{{ $cashflow->balance }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"></script>
@endsection
