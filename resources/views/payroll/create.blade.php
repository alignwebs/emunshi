@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
             @include('flash::message')
            <div class="card">
                <div class="card-header">Create Payroll</div>

                <div class="card-body">
                    {!! Form::open(['route' => 'payroll.store']) !!}
                        
                        <input type="hidden" name="branch_id" value="1">
                        <input type="hidden" name="employee_id" value="1">

                        <div class="form-group">
                            <label>Name</label>
                            {!! Form::text('name', null, ['class' => 'form-control','required'=>true]) !!}
                        </div>

                        <div class="form-group">
                            <label>Date</label>
                            {!! Form::date('date', \Carbon\Carbon::now(),['class' => 'form-control', 'id' => 'date', 'onchange' => 'getNoOfDaysInMonth()']) !!}
                        </div>

                        <div class="form-group">
                            <label>Days</label>
                            {!! Form::number('days', null,['class' => 'form-control', 'id' => 'days', 'onchange' => 'calculateDays()', 'readonly' => true]) !!}
                        </div>

                        <div class="form-group">
                            <label>Absence</label>
                            {!! Form::number('absence', null,['class' => 'form-control', 'id' => 'absence', 'onchange' => 'calculateDays()','required'=>true]) !!}
                        </div>
                        <div class="form-group">
                            <label>Worked</label>
                            {!! Form::number('worked', null,['class' => 'form-control', 'id' => 'worked', 'onchange' => 'calculateDays()','required'=>true]) !!}
                        </div>
                        <div class="form-group">
                            <label>Week Off</label>
                            {!! Form::number('week_off', null,['class' => 'form-control', 'id' => 'week_off', 'onchange' => 'calculateDays()','required'=>true]) !!}
                        </div>
                        <div class="form-group">
                            <label>Total Days</label>
                            {!! Form::number('total_days', null,['class' => 'form-control', 'id' => 'total_days', 'onchange' => 'calculateTotalAmount()', 'readonly' => true]) !!}
                        </div>

                        <div class="form-group">
                            <label>Rate</label>
                            {!! Form::number('rate', null,['class' => 'form-control', 'step' => '0.01', 'id' => 'rate', 'onchange' => 'calculateTotalAmount()','required'=>true]) !!}
                        </div>

                        <div class="form-group">
                            <label>Salary</label>
                            {!! Form::number('salary', null,['class' => 'form-control', 'step' => '0.01', 'id' => 'salary', 'readonly' => true]) !!}
                        </div>
                        
                        <br>

                        <button class="btn btn-block btn-primary" type="submit">Submit</button>


                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    jQuery(document).ready(function () {
        getNoOfDaysInMonth();
    })

    function calculateTotalAmount()
    {
        let total_days = document.getElementById('total_days').value;
        let rate = document.getElementById('rate').value;

        let total = total_days * rate;

        document.getElementById('salary').value = total;
    }

    function getNoOfDaysInMonth()
    {
         let date = document.getElementById('date').value;
         date = date.split("-")
         let d = new Date(date[2], date[1], 0).getDate(); 

         document.getElementById('days').value = d;

         calculateDays()
    }

    function calculateDays()
    {
        let days = document.getElementById('days').value;
        let absence = document.getElementById('absence').value;
        let week_off = document.getElementById('week_off').value;
        let total_days = (days - absence) + +week_off;
        let worked = days - absence;
        
        document.getElementById('total_days').value = total_days;
        document.getElementById('worked').value = worked;

        calculateTotalAmount();
    }
</script>
@endsection
