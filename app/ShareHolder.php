<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareHolder extends Model
{
    public function person()
    {
        return $this->belongsTo('App\Person','people_id');
    }
    public function document()
    {
        return $this->morphMany('App\Document', 'documentable');
    }
}
