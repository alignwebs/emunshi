<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayRoll extends Model
{
    public function employee()
    {
        return $this->belongsTo('App\Employee','employee_id');
    }
    public function document()
    {
        return $this->morphMany('App\Document', 'documentable');
    }
}
