<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BalanceInventory extends Model
{
    public function document()
    {
        return $this->morphMany('App\Document', 'documentable');
    }
}
