<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public static function type($type)
    {
		return (string) self::where('type',$type)->first()->value;
    }
}
