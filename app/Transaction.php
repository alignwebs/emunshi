<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
   public function document()
    {
        return $this->morphMany('App\Document', 'documentable');
    }
}
