<?php

namespace App\Http\Controllers;

use App\PayRoll;
use Illuminate\Http\Request;
use Carbon\Carbon;

class PayRollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('payroll.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Payroll::insert($request->except(['_token']));
        flash('Payroll created successfully')->success();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PayRoll  $payRoll
     * @return \Illuminate\Http\Response
     */
    public function show(PayRoll $payRoll)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PayRoll  $payRoll
     * @return \Illuminate\Http\Response
     */
    public function edit(PayRoll $payRoll)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PayRoll  $payRoll
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PayRoll $payRoll)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PayRoll  $payRoll
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayRoll $payRoll)
    {
        //
    }
}
