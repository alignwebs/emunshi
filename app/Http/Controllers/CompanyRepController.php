<?php

namespace App\Http\Controllers;

use App\CompanyRep;
use Illuminate\Http\Request;

class CompanyRepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyRep  $companyRep
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyRep $companyRep)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyRep  $companyRep
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyRep $companyRep)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyRep  $companyRep
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyRep $companyRep)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyRep  $companyRep
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyRep $companyRep)
    {
        //
    }
}
