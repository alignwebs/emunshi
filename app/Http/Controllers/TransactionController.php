<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Receiving;
use App\Giving;
use App\Person;
use App\PayRoll;
use App\BalanceInventory;
use Illuminate\Http\Request;
use DB;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['receiving'] = Receiving::selectRaw('SUM(amount) as balance, `from`')->groupBy('from')->get();

        $data['transaction'] = Transaction::selectRaw('SUM(amount) as paid, `to_company`')->groupBy('to_company')->get();
        
        $data['cashflow'] = DB::select('SELECT 
                                                SUM(r.amount) as r_amount, 
                                                SUM(t.amount) as t_amount, 
                                                SUM(r.amount-t.amount) as balance,
                                                r.from as company  
                                                FROM receivings as r 
                                                LEFT JOIN transactions as t ON(r.from = t.to_company) 
                                                GROUP BY r.from HAVING balance != 0');

        $data['credit_sales'] = DB::select('SELECT 
                                                SUM(g.amount) as g_amount, 
                                                SUM(t.amount) as t_amount, 
                                                SUM(g.amount-t.amount) as balance,
                                                g.to as company  
                                                FROM givings as g 
                                                LEFT JOIN transactions as t ON(g.to = t.from) 
                                                GROUP BY g.to HAVING g.to != "Cash"');

        $data['payroll'] = DB::select('SELECT 
                                                SUM(pr.salary) as salary, 
                                                SUM(t.amount) as paid, 
                                                SUM(pr.salary-t.amount) as balance,
                                                pr.name as employee  
                                                FROM pay_rolls as pr
                                                LEFT JOIN transactions as t ON(pr.name = t.to_company) 
                                                GROUP BY pr.name');

        
        // $data['cashinhand'] = DB::select('SELECT SUM(amount) as amount, `from` as party from transactions GROUP BY `from`');
        $got = Transaction::select('from','to_company','amount')->where('dic','T')->get();
        // $got = Transaction::selectRaw('SUM(amount) as amount, `from` as from_party, `to_company` as to_party')->groupBy('from', 'to_company')->get();
        // $paid = Transaction::selectRaw('SUM(amount) as amount, `from` as from_party, `to_company` as to_party')->groupBy('to_company', 'from')->get();

        
        foreach ($got as $row) {

            $paid = Transaction::where('from', $row->to_company)->get();
            $amount = 0;
            foreach($paid as $a)
            {
                $amount += $a->amount;
            }
            
           $data['cashinhand'][] = ['party' =>  $row->to_company, 'amount' => $row->amount - $amount];
            // $paid_amount = 0;
            // if($paid_p)
            //     $paid_amount = $paid_p->amount;

            // $data['cashinhand'][] = ['party' =>  $row->from_party, 'amount' => $row->amount - $paid_amount];
         
        }
       
        // $cashinhand = $total - $cashinhand;
        // return $data['cashinhand'];
        return view('cashflow', compact('data'));
    }

    public function profitLoss()
    {
        $data['receiving'] = Receiving::selectRaw('SUM(amount) as balance, `from`')->groupBy('from')->get();
        
        $data['sales'] = Giving::selectRaw('SUM(amount) as balance, `sub_type`')->groupBy('sub_type')->get();
        // $data['sales'] = BalacenInt::selectRaw('SUM(amount) as balance, `sub_type`')->groupBy('sub_type')->get();

        $data['transaction'] = Transaction::selectRaw('SUM(amount) as paid, `to_company`')->groupBy('to_company')->get();
        
        $data['cashflow'] = DB::select('SELECT 
                                                SUM(r.amount) as r_amount, 
                                                SUM(t.amount) as t_amount, 
                                                SUM(r.amount-t.amount) as balance,
                                                r.from as company  
                                                FROM receivings as r 
                                                LEFT JOIN transactions as t ON(r.from = t.to_company) 
                                                GROUP BY r.from HAVING balance != 0');

        $data['credit_sales'] = DB::select('SELECT 
                                                SUM(g.amount) as g_amount, 
                                                SUM(t.amount) as t_amount, 
                                                SUM(g.amount-t.amount) as balance,
                                                g.to as company  
                                                FROM givings as g 
                                                LEFT JOIN transactions as t ON(g.to = t.from) 
                                                GROUP BY g.to HAVING g.to != "Cash"');

        $data['payroll'] = DB::select('SELECT 
                                                SUM(pr.salary) as salary, 
                                                SUM(t.amount) as paid, 
                                                SUM(pr.salary-t.amount) as balance,
                                                pr.name as employee  
                                                FROM pay_rolls as pr
                                                LEFT JOIN transactions as t ON(pr.name = t.to_company) 
                                                GROUP BY pr.name');

        
        // $data['cashinhand'] = DB::select('SELECT SUM(amount) as amount, `from` as party from transactions GROUP BY `from`');
        $got = Transaction::select('from','to_company','amount')->where('dic','T')->get();
        // $got = Transaction::selectRaw('SUM(amount) as amount, `from` as from_party, `to_company` as to_party')->groupBy('from', 'to_company')->get();
        // $paid = Transaction::selectRaw('SUM(amount) as amount, `from` as from_party, `to_company` as to_party')->groupBy('to_company', 'from')->get();

        
        foreach ($got as $row) {

            $paid = Transaction::where('from', $row->to_company)->get();
            $amount = 0;
            foreach($paid as $a)
            {
                $amount += $a->amount;
            }
            
           $data['cashinhand'][] = ['party' =>  $row->to_company, 'amount' => $row->amount - $amount];
            // $paid_amount = 0;
            // if($paid_p)
            //     $paid_amount = $paid_p->amount;

            // $data['cashinhand'][] = ['party' =>  $row->from_party, 'amount' => $row->amount - $paid_amount];
         
        }
        
        // $cashinhand = $total - $cashinhand;
        // return $data['cashinhand'];
        return view('pl', compact('data'));
    }

    public function calucalteProfitLoss(Request $request)
    {   
        $from = date('Y-m-d', strtotime('-1 day', strtotime($request->from)));
        $to = date('Y-m-d',strtotime($request->to));

        // SALES
        $data['sales'] = Giving::selectRaw('SUM(amount) as balance, `sub_type`')->whereBetween('date', [$from, $to])->groupBy('sub_type')->get();

        // DIRECT EXPENSE
        $starting = BalanceInventory::where('date',$from)->first();
        $ending = BalanceInventory::where('date',$to)->first();

        if(empty($starting) || empty($ending))
            return 'Data not found';
        else{

            $data['startingBalance'] = $starting->amount;
            $data['endingingBalance'] = $ending->amount;
            $data['purchase'] = Receiving::selectRaw('SUM(amount) as balance')->whereBetween('date', [$from, $to])->where('dic','D')->first();
            $data['totalDirectExpense'] = ($data['startingBalance'] + $data['purchase']->balance) - $data['endingingBalance'];
        }

   

        $data['indirectExpenses'] = Receiving::selectRaw('SUM(amount) as balance, `from`,`dic`')->whereBetween('date', [$from, $to])->groupBy('from','dic')->having('dic','I')->get();
        $data['salary'] = PayRoll::selectRaw('SUM(salary) as balance')->whereBetween('date', [$from, $to])->first();
         
         return view('ajax.ajax_pl', compact('data'));       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['people'] = Person::all()->pluck('name', 'id');
        return view('transaction.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Transaction::insert($request->except(['_token']));
        flash('Transaction created successfully')->success();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
