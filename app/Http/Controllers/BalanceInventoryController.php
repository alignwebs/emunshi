<?php

namespace App\Http\Controllers;

use App\BalanceInventory;
use Illuminate\Http\Request;

class BalanceInventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BalanceInventory  $balanceInventory
     * @return \Illuminate\Http\Response
     */
    public function show(BalanceInventory $balanceInventory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BalanceInventory  $balanceInventory
     * @return \Illuminate\Http\Response
     */
    public function edit(BalanceInventory $balanceInventory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BalanceInventory  $balanceInventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BalanceInventory $balanceInventory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BalanceInventory  $balanceInventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(BalanceInventory $balanceInventory)
    {
        //
    }
}
