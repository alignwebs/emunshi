<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function reps()
    {
        return $this->hasMany('App\CompanyRep');
    }

    public function document()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

    public function branch()
    {
        return $this->hasMany('App\Branch');
    }
}
