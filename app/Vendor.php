<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    public function person()
    {
        return $this->belongsTo('App\Person','people_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function document()
    {
        return $this->morphMany('App\Document', 'documentable');
    }
}
