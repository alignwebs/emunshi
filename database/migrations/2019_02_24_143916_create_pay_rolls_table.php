<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayRollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_rolls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('branch_id');
            $table->integer('employee_id');
            $table->date('date');
            $table->integer('days');
            $table->integer('absence');
            $table->integer('worked');
            $table->integer('week_off');
            $table->integer('total_days');
            $table->double('rate',8,2);
            $table->double('salary',8,2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_rolls');
    }
}
