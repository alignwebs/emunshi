<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receivings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('branch_id');
            $table->string('type')->nullable();
            $table->string('sub_type')->nullable();//delete
            $table->string('sub_type2')->nullable();//delete
            $table->string('dic');
            $table->date('date');
            $table->date('effective_date');
            $table->string('category')->nullable();
            $table->string('desc');
            $table->string('from');
            $table->integer('from_company_id')->nullable();
            $table->float('qty',8,2);
            $table->string('unit');
            $table->double('unit_price',8,2);
            $table->double('amount',8,2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receivings');
    }
}
