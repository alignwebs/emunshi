<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('branch_id');
            $table->date('date');
            $table->string('dic');
            $table->string('desc');
            $table->float('qty',8,2);
            $table->string('unit');
            $table->double('price',8,2);
            $table->double('amount',8,2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_inventories');
    }
}
