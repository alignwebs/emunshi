<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('branch_id');
            $table->string('type')->nullable();
            $table->string('sub_type')->nullable();
            $table->string('sub_type2')->nullable();
            $table->string('dic');
            $table->date('date');
            $table->date('effective_date');
            $table->string('category')->nullable();
            $table->string('desc');
            $table->integer('from_person_id')->nullable();
            $table->float('qty',8,2)->nullable();
            $table->string('unit')->nullable();
            $table->double('unit_price',10,2)->nullable();
            $table->double('amount',10,2);
            $table->integer('to_person_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
