<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('receiving', 'ReceivingController');
Route::resource('transaction', 'TransactionController');
Route::resource('payroll', 'PayRollController');


Route::get('/pl', 'TransactionController@profitLoss')->name('pl');
Route::post('/pl', 'TransactionController@calucalteProfitLoss')->name('pl.calculate');

